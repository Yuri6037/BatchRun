#![allow(non_snake_case)]

/*
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

use std::path::{Path, PathBuf};
use std::fs::ReadDir;
use std::time::Instant;

fn thread_body(base_cmd: &str, args: &[String], file: PathBuf)
{
    let sfile = file.to_str().unwrap(); // Only allow UTF-8 paths because in main we only consider UTF-8 strings
    let mut cmd = std::process::Command::new(base_cmd);
    for s in args {
        let s1 = s.replace("$1", sfile);
        cmd.arg(s1);
    }
    match cmd.status()
    {
        Err(e) => {
            eprintln!("Failed to run command on '{}': {}", sfile, e);
        },
        Ok(e) => {
            if !e.success()
            {
                eprintln!("Failed to run command on '{}': {}", sfile, e);
                std::process::exit(1);
            }
        }
    }
}

fn batch_run(mut size: usize, iter: &mut ReadDir, base_cmd: &str, args: &[String]) -> std::io::Result<bool>
{
    crossbeam::scope(|scope| {
        while size > 0 {
            if let Some(file) = iter.next() {
                let f = file?.path();
                scope.spawn(|_| {
                    thread_body(&base_cmd, args, f);
                });
            } else {
                return Ok(false);
            }
            size -= 1;
        }
        return Ok(true);
    }).unwrap()
}

fn iter_directory(maxt: usize, dir: &Path, base_cmd: &str, args: &[String]) -> std::io::Result<()>
{
    let mut iter = std::fs::read_dir(Path::new(dir))?;

    while batch_run(maxt, &mut iter, base_cmd, args)? {}
    return Ok(());
}

fn main()
{
    let mut args = std::env::args();
    args.next();
    if let (Some(tcount), Some(dir), Some(base_cmd)) = (args.next(), args.next(), args.next()) {
        let num: usize = match tcount.parse() {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Could not read maximum task number: {}", e);
                std::process::exit(1);
            }
        };
        let remaining: Vec<String> = args.collect();
        let time = Instant::now();
        if let Err(e) = iter_directory(num, Path::new(&dir), &base_cmd, &remaining) {
            eprintln!("IO Error: {}", e);
            std::process::exit(1);
        }
        let secs = time.elapsed().as_secs();
        println!("I'm done, batch took {} seconds to run!", secs);
    } else {
        eprintln!("Usage: <maximum task count> <directory> command line with or without spaces");
        std::process::exit(2);
    }
}
